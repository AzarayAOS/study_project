﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LogBookServerApp.Service
{
    interface IService<T>
    {
        Task<T> Create(T document);
        Task<T> Delete(T document);
        Task<List<T>> GetAllAsync();
    }
}
