﻿using LogBookServerApp.Data;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
namespace LogBookServerApp.Service
{
    public class PatternService<T> : IService<T>
    {
        public HttpClient HttpClient { get; }
        public AppSettings AppSettings { get; }

        public PatternService(HttpClient httpClient, IOptions<AppSettings> appSettings)
        {
            AppSettings = appSettings.Value;

            httpClient.BaseAddress = new Uri(AppSettings.AddressSever);
            httpClient.DefaultRequestHeaders.Add("User-Agent", "BlazorServer");

            HttpClient = httpClient;
        }

        public async Task<T> Create(T pattern)
        {
            string serializedUser = JsonConvert.SerializeObject(pattern);

            var requestMessage = new HttpRequestMessage(HttpMethod.Post, "Pattern")
            {
                Content = new StringContent(serializedUser)
            };

            requestMessage.Content.Headers.ContentType
                = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await HttpClient.SendAsync(requestMessage);
            _ = response.StatusCode;
            var responseBody = await response.Content.ReadAsStringAsync();

            var returnedPattern = JsonConvert.DeserializeObject<dynamic>(responseBody);

            return await Task.FromResult(returnedPattern);
        }

        public async Task<List<T>> GetAllAsync()
        {
            var requestMessage = new HttpRequestMessage(HttpMethod.Get, AppSettings.AddressSever + "Pattern/GetPattern");

            var response = await HttpClient.SendAsync(requestMessage);

            var responseStatusCode = response.StatusCode;

            if (responseStatusCode.ToString() == "OK")
            {
                var responseBody = await response.Content.ReadAsStringAsync();
                return await Task.FromResult(JsonConvert.DeserializeObject<List<T>>(responseBody));
            }
            else
                return null;
        }

        public Task<T> Delete(T document)
        {
            throw new NotImplementedException();
        }
    }
}
