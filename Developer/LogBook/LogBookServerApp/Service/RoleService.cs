﻿using LogBookServerApp.Data;
using Microsoft.AspNetCore.Routing.Template;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using LogBookModels;

namespace LogBookServerApp.Service
{
    public class RoleService
    {
        public HttpClient HttpClient { get; }
        public AppSettings AppSettings { get; }

        public RoleService(HttpClient httpClient, IOptions<AppSettings> appSettings)
        {
            AppSettings = appSettings.Value;

            if (httpClient.BaseAddress is null)
            {
                httpClient.BaseAddress = new Uri(AppSettings.AddressSever);
                httpClient.DefaultRequestHeaders.Add("User-Agent", "BlazorServer");
            }

            HttpClient = httpClient;
        }

        public async Task<Role> Create(Role role)
        {
            string serializedUser = JsonConvert.SerializeObject(role);

            var requestMessage = new HttpRequestMessage(HttpMethod.Post, "Role")
            {
                Content = new StringContent(serializedUser)
            };

            requestMessage.Content.Headers.ContentType
                = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await HttpClient.SendAsync(requestMessage);
            var responseStatusCode = response.StatusCode;
            var responseBody = await response.Content.ReadAsStringAsync();

            var returnedUser = JsonConvert.DeserializeObject<Role>(responseBody);

            return await Task.FromResult(returnedUser);
        }
        public async Task<Role> DeleteAsync(Role item)
        {
            //user.Password = Utility.Encrypt(user.Password);
            string serializedUser = JsonConvert.SerializeObject(item);

            var requestMessage = new HttpRequestMessage(HttpMethod.Post, "Role/Delete");
            requestMessage.Content = new StringContent(serializedUser);

            requestMessage.Content.Headers.ContentType
                = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await HttpClient.SendAsync(requestMessage);

            return await Task.FromResult(item);
        }
        public async Task<List<Role>> GetAll()
        {
            var requestMessage = new HttpRequestMessage(HttpMethod.Get, "Role/GetRoles");       //GetRoles
            var response = await HttpClient.SendAsync(requestMessage);

            var responseStatusCode = response.StatusCode;
            var responseBody = await response.Content.ReadAsStringAsync();

            var returnedUser = JsonConvert.DeserializeObject<List<Role>>(responseBody);
            return await Task.FromResult(returnedUser);
        }
        public async  Task<List<Role>> GetAllsAsync(CancellationToken ct = default)
        {
            return await GetAll();
        }
        public Task<IEnumerable<string>> GetListRole(CancellationToken ct = default)
        {
            IEnumerable<string> listRoles = GetAll().Result.Select(x => x.RoleName).ToList();
            return Task.FromResult(listRoles);
        }
        public async Task Insert(IDictionary<string, object> newValue)
        {
            var item = new Role();
            await UpdateInternal(item, newValue);
            await Create(item);
        }
        public async Task<Role> UpdateInternal(Role item, IDictionary<string, object> newValue)
        {
            foreach (var field in newValue.Keys)
            {
                switch (field)
                {
                    case nameof(item.RoleName):
                        item.RoleName = (string)newValue[field];
                        break;
                }
            }
            await UpdateAsync(item);
            return await Task.FromResult(item);
        }
        private async Task<Role> UpdateAsync(Role item)
        {
            string serializedUser = JsonConvert.SerializeObject(item);

            var requestMessage = new HttpRequestMessage(HttpMethod.Post, "Role/Update");

            requestMessage.Content = new StringContent(serializedUser);
            requestMessage.Content.Headers.ContentType
                = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await HttpClient.SendAsync(requestMessage);

            var responseStatusCode = response.StatusCode;
            var responseBody = await response.Content.ReadAsStringAsync();

            var returnObj = JsonConvert.DeserializeObject<Role>(responseBody);

            return await Task.FromResult(returnObj);
        }

        internal Role GetByName(string v)
        {
            throw new NotImplementedException();
        }
    }
}
