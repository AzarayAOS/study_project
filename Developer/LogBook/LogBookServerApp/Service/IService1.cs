﻿using LogBookModels.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LogBookServerApp.Service
{
    interface IService1<T>
    {
        Task<T> Create(T document);
        Task<T> Delete(T document);
        Task<List<T>> GetAllAsync();
    }
}
