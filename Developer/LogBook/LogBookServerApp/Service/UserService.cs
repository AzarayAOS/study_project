﻿using LogBookModels;
using LogBookServerApp.Data;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace LogBookServerApp.Service
{
    public class UserService
    {

        //[Inject] RoleService roleService { get; set; }

        public HttpClient HttpClient { get; }

        private RoleService _roleservice;

        public AppSettings AppSettings { get; }

        public IEnumerable<User> Users { get; set; }

        public UserService(HttpClient httpClient, IOptions<AppSettings> appSettings, RoleService roleService)
        {
            _roleservice = roleService;
            AppSettings = appSettings.Value;
            if (httpClient.BaseAddress is null)
            {
                httpClient.BaseAddress = new Uri(AppSettings.AddressSever);
                httpClient.DefaultRequestHeaders.Add("User-Agent", "BlazorServer");
            }
            HttpClient = httpClient;
        }

        public async Task<User> Create(User user)
        {
            string serializedUser = JsonConvert.SerializeObject(user);

            var requestMessage = new HttpRequestMessage(HttpMethod.Post, "User")
            {
                Content = new StringContent(serializedUser)
            };

            requestMessage.Content.Headers.ContentType
                = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await HttpClient.SendAsync(requestMessage);
            _ = response.StatusCode;
            var responseBody = await response.Content.ReadAsStringAsync();

            var returnedUser = JsonConvert.DeserializeObject<User>(responseBody);

            return await Task.FromResult(returnedUser);
        }

        public async Task<List<User>> GetAllAsync()
        {
            var requestMessage = new HttpRequestMessage(HttpMethod.Get, AppSettings.AddressSever + "user/GetUsers");

            var response = await HttpClient.SendAsync(requestMessage);

            var responseStatusCode = response.StatusCode;

            if (responseStatusCode.ToString() == "OK")
            {
                var responseBody = await response.Content.ReadAsStringAsync();
                return await Task.FromResult(JsonConvert.DeserializeObject<List<User>>(responseBody));
            }
            else
                return null;
        }

        public Task<User> Delete(User user)
        {
            //user.Password = Utility.Encrypt(user.Password);
            string serializedUser = JsonConvert.SerializeObject(user);

            var requestMessage = new HttpRequestMessage(HttpMethod.Post, "User/Delete");
            requestMessage.Content = new StringContent(serializedUser);

            requestMessage.Content.Headers.ContentType
                = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = HttpClient.SendAsync(requestMessage);

            return Task.FromResult(user);
        }

        private async Task<User> UpdateInternal(User dataItem, IDictionary<string, object> newValue)
        {
            foreach (var field in newValue.Keys)
            {
                switch (field)
                {
                    case nameof(dataItem.FirstName):
                        dataItem.FirstName = (string)newValue[field];
                        break;
                    case nameof(dataItem.LastName):
                        dataItem.LastName = (string)newValue[field];
                        break;
                    case nameof(dataItem.Phone):
                        dataItem.Phone = (string)newValue[field];
                        break;
                    case nameof(dataItem.CorporatePhone):
                        dataItem.CorporatePhone = (string)newValue[field];
                        break;
                    case nameof(dataItem.FlagWorking):
                        dataItem.FlagWorking = (bool)newValue[field];
                        break;
                    case nameof(dataItem.Birthday):
                        dataItem.Birthday = (DateTime)newValue[field];
                        break;
                    case nameof(dataItem.Password):
                        dataItem.Password = (string)newValue[field];
                        break;
                    case nameof(dataItem.role):
                        //IEnumerable<Role> result = await _roleservice.GetAllsAsync();
                        //dataItem.role = result.FirstOrDefault(x => x.RoleName == (string)newValue[field]);
                        break;
                        //case nameof(dataItem.company):
                        //    IEnumerable<Company> companys = await CompanyService.GetAll();
                        //    dataItem.company = companys.FirstOrDefault(x => x.Name == (string)newValue[field]);
                        //    break;
                }
            }
            return await Task.FromResult(dataItem);
        }

        public async Task Insert(IDictionary<string, object> newValue)
        {
            var dataItem = new User();
            dataItem = UpdateInternal(dataItem, newValue).Result;
            await Create(dataItem);
        }
        public Task<List<User>> GetAll(CancellationToken ct = default)
        {
            return GetAllAsync();
        }
        public async Task Update(User dataItem, IDictionary<string, object> newValue)
        {
            await UpdateInternal(dataItem, newValue);

            await UpdateUserAsync(dataItem);
        }
        private async Task<User> UpdateUserAsync(User user)
        {
            string serializedUser = JsonConvert.SerializeObject(user);

            var requestMessage = new HttpRequestMessage(HttpMethod.Post, "User/Update");

            requestMessage.Content = new StringContent(serializedUser);
            requestMessage.Content.Headers.ContentType
                = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await HttpClient.SendAsync(requestMessage);

            var responseStatusCode = response.StatusCode;
            var responseBody = await response.Content.ReadAsStringAsync();

            var returnedUser = JsonConvert.DeserializeObject<User>(responseBody);

            return await Task.FromResult(returnedUser);
        }
        public async Task<(User, HttpStatusCode)> RegisterUserAsync(User user)
        {
            //user.Password = Utility.Encrypt(user.Password);
            string serializedUser = JsonConvert.SerializeObject(user);

            var requestMessage = new HttpRequestMessage(HttpMethod.Post, "User/RegisterUser");
            requestMessage.Content = new StringContent(serializedUser);

            requestMessage.Content.Headers.ContentType
                = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await HttpClient.SendAsync(requestMessage);

            var responseStatusCode = response.StatusCode;
            var responseBody = await response.Content.ReadAsStringAsync();

            var returnedUser = JsonConvert.DeserializeObject<User>(responseBody);

            return await Task.FromResult((returnedUser, responseStatusCode));
        }

        public async Task<(User, HttpStatusCode)> LoginAsync(User user)
        {
            string serializedUser = JsonConvert.SerializeObject(user);

            var requestMessage = new HttpRequestMessage(HttpMethod.Get, "User/Login");
            requestMessage.Content = new StringContent(serializedUser);

            requestMessage.Content.Headers.ContentType
                = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await HttpClient.SendAsync(requestMessage);

            var responseStatusCode = response.StatusCode;

            var responseBody = await response.Content.ReadAsStringAsync();

            var returnedUser = JsonConvert.DeserializeObject<User>(responseBody);

            return await Task.FromResult((returnedUser, responseStatusCode));

        }

        public async Task<User> GetUserByAccessTokenAsync(string accessToken)
        {
            string serializedRefreshRequest = JsonConvert.SerializeObject(accessToken);

            var requestMessage = new HttpRequestMessage(HttpMethod.Post, "User/GetUserByAccessToken");
            requestMessage.Content = new StringContent(serializedRefreshRequest);

            requestMessage.Content.Headers.ContentType
                = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await HttpClient.SendAsync(requestMessage);

            var responseStatusCode = response.StatusCode;
            var responseBody = await response.Content.ReadAsStringAsync();

            var returnedUser = JsonConvert.DeserializeObject<User>(responseBody);

            return await Task.FromResult(returnedUser);
        }
    }
}
