﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogBookServerApp.Models
{
    public class Edit
    {
        public string Label { get; set; }
        public string DefaultText { get; set; }
    }
}
