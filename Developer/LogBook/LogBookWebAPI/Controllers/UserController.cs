﻿using System.Collections.Generic;
using LogBookStores.Data;
using LogBookModels;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Linq;
using System.Security.Cryptography;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Options;
using LogBookAPI.Utils;
using System.Net;

namespace LogBookStores.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
  
    public class UserController : ControllerBase
    {
        private readonly JWTSettings _jwtsettings;
        private readonly UserService _userService;

        public UserController(UserService userService, IOptions<JWTSettings> jwtsettings)
        {
            _userService = userService;
            _jwtsettings = jwtsettings.Value;
        }

        [HttpGet("GetUsers")]
        public async Task<ActionResult<List<User>>> Get() =>
           await _userService.Get();

        [HttpPost("GetUserByAccessToken")]
        public async Task<ActionResult<User>> GetUserByAccessToken([FromBody] string refreshToken)
        {
            User user = _userService.GetBy((User u) => u.RefreshToken == refreshToken).FirstOrDefault();
            return await Task.FromResult(user);
        }
        [HttpPost("RegisterUser")]
        public async Task<ActionResult<User>> Get(User regUser)
        {
            User user = _userService.GetBy((User u) => u.Login == regUser.Login).FirstOrDefault();
            if (user is null)
            {

                regUser.RefreshToken = GenerateRefreshToken().Token;
                regUser.PasswordHash = MD5Generator.GetHash(regUser.Password);
                _userService.Create(regUser);
                return await Task.FromResult(regUser);
            }
            else
            {
                return StatusCode(409);
            }
        }
        [HttpGet("Login")]
        public async Task<ActionResult<User>> Login(User regUser)
        {
            regUser.PasswordHash = MD5Generator.GetHash(regUser.Password);

            User user = _userService.GetBy((User u) => u.Login == regUser.Login).FirstOrDefault();
            
            if (user != null)
            {
                if (user.PasswordHash == regUser.PasswordHash)
                {
                    if ((string.IsNullOrEmpty(user.RefreshToken)))
                    {
                        user.RefreshToken = GenerateRefreshToken().Token;
                        _userService.Update(user.Id, user);
                    }
                    return await Task<User>.FromResult(user);
                }
            }
            return StatusCode(401);
        }

        [HttpGet("{id:length(24)}", Name = "GetUser")]
        public ActionResult<User> Get(string id)
        {
            var user = _userService.Get(id);

            if (user == null)
            {
                return NotFound();
            }

            return user;
        }

        [HttpPost]
        public ActionResult<User> Create(User user)
        {
            _userService.Create(user);

            return CreatedAtRoute("Getuser", new { id = user.Id.ToString() }, user);
        }

        [HttpPost("Delete")]
        public void Delete([FromBody] User user)
        {
            _userService.Remove(user);
        }

        [HttpPost("Update")]
        public ActionResult<User> Update([FromBody] User user)
        {
            _userService.Update(user.Id, user);

            return _userService.Get(user.Id);
        }

        private RefreshToken GenerateRefreshToken()
        {
            RefreshToken refreshToken = new RefreshToken();

            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                refreshToken.Token = Convert.ToBase64String(randomNumber);
            }
            refreshToken.ExpiryDate = DateTime.UtcNow.AddMonths(6);

            return refreshToken;
        }

        private string GenerateAccessToken(int userId)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_jwtsettings.SecretKey);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, Convert.ToString(userId))
                }),
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }


    }
}
