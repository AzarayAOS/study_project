﻿using LogBookAPI.Data.Service;
using LogBookModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogBookAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RoleController : ControllerBase
    {
        private readonly ObjectService<Role> _service;

        public RoleController(ObjectService<Role> service)
        {
            _service = service;
        }

        [HttpGet("GetRoles")]
        public async Task<ActionResult<List<Role>>> Get() =>
           await _service.Get();

        [HttpGet("{id:length(24)}", Name = "GetRole")]
        public async Task<ActionResult<Role>> Get(string id)
        {
            var obj = _service.Get(id);

            if (obj == null)
            {
                return NotFound();
            }

            return await obj;
        }

        [HttpPost]
        public async Task< ActionResult<Role>> Create(Role role)
        {
             await _service.Create(role);

            return CreatedAtRoute("Getrole", new { id = role.Id.ToString() }, role);
        }

        [HttpPost("Delete")]
        public void Delete([FromBody] Role role)
        {
             _service.Remove(role);
        }

        [HttpPost("Update")]
        public async Task<ActionResult<Role>> Update([FromBody] Role user)
        {
             _service.Update(user.Id, user);

            return  await _service.Get(user.Id);
        }
    }
}
