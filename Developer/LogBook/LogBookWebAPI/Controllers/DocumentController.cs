﻿using System.Collections.Generic;
using LogBookStores.Service;
using LogBookModels.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LogBookStores.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DocumentController : Controller
    {
        private readonly DocumentService _documentService;

        public DocumentController(DocumentService documentService)
        {
            _documentService = documentService;
        }

        [HttpGet("GetDocument")]
        public async Task<ActionResult<List<Template>>> Get() =>
           await _documentService.Get();

        [HttpGet("{id:length(24)}", Name = "GetDocument")]
        public ActionResult<Template> Get(string id)
        {
            var document = _documentService.Get(id);

            if (document == null)
            {
                return NotFound();
            }

            return document;
        }

        [HttpPost]
        public ActionResult<Template> Create(Template document)
        {
            _documentService.Create(document);

            return CreatedAtRoute("Getdocument", new { id = document.Id.ToString() }, document);
        }
    }
}
