﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LogBookAPI.Data.Service;
using LogBookModels.Models;
using Microsoft.AspNetCore.Mvc;

namespace LogBookStores.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class PatternController : ControllerBase
    {
        private readonly PatternService _patternService;

        public PatternController(PatternService patternService)
        {
            _patternService = patternService;
        }

        [HttpGet("GetPattern")]
        public async Task<ActionResult<List<Template>>> Get() =>
           await _patternService.Get();

        [HttpPost]
        public ActionResult<dynamic> Create(Template pattern)
        {
            _patternService.Create(pattern);

            return CreatedAtRoute("Getpatter", pattern);
        }
        public class Part
        {
            public string PartName { get; set; }

            public int PartId { get; set; }
        }
    }
}
