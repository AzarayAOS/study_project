﻿using LogBookModels.Models;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LogBookAPI.Data.Service
{
    public class PatternService 
    {
        private readonly IMongoCollection<Template> _document;

        public PatternService(IDatabaseSettings settings)
        {
            var client = new MongoClient(settings.DBConnection);
            var database = client.GetDatabase(settings.DatabaseName);

            _document = database.GetCollection<Template>("Pattern");
        }

        public async Task<List<Template>> Get() =>
           await _document.Find(document => true).ToListAsync();

        public dynamic Create(Template document)
        {
            _document.InsertOne(document);
            return document;
        }

        public void Remove(string id) =>
            _document.DeleteOne(document => document.Id == id);
    }
}
