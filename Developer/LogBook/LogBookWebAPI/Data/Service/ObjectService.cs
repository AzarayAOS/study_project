﻿using LogBookModels;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LogBookModels.Models;

namespace LogBookAPI.Data.Service
{
    public class ObjectService<T> where T : BaseObject
    {
        private readonly IMongoCollection<T> _objects;
        public ObjectService(IDatabaseSettings settings)
        {
            var client = new MongoClient(settings.DBConnection);
            var database = client.GetDatabase(settings.DatabaseName);

            _objects = database.GetCollection<T>(typeof(T).Name.ToString());
        }

        public async Task<List<T>> Get() =>
            await _objects.Find(u => true).ToListAsync();

        public async Task<T> Get(string id) =>
            await _objects.Find<T>(t => t.Id == id).FirstOrDefaultAsync();

        public async  Task<T> Create(T user)
        {
            await _objects.InsertOneAsync(user);
            return user;
        }

        public async void Update(string id, T obj) =>
            await _objects.ReplaceOneAsync(t => t.Id == id, obj);

        public async void Remove(T obj) =>
           await _objects.DeleteOneAsync(u => u.Id == obj.Id);

        public async void Remove(string id) =>
           await _objects.DeleteOneAsync(t => t.Id == id);
    }
}
