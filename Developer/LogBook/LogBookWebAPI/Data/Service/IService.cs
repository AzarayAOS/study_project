﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LogBookAPI.Data.Service
{
    interface IService<T>
    {
        Task <List<T>> Get();
        T Get(string id);
        void Update(string id, T value);
        void Remove(T value);
        void Remove(string id);
    }
}
