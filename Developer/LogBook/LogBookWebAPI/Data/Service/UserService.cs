﻿using LogBookAPI.Data.Service;
using LogBookModels;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LogBookModels.Models;

namespace LogBookStores.Data
{
    public class UserService: IService<User>
    {
        private readonly IMongoCollection<User> _users;

        public UserService(IDatabaseSettings settings)
        {
            var client = new MongoClient(settings.DBConnection);
            var database = client.GetDatabase(settings.DatabaseName);

            _users = database.GetCollection<User>(settings.UsersCollectionName);
        }

        public async Task<List<User>> Get()  =>
            await _users.Find(user => true).ToListAsync();
     
        public  User Get(string id) =>
            _users.Find<User>(user => user.Id == id).FirstOrDefault();

        public IEnumerable<User> GetBy(Expression<Func<User, bool>> predicate)
        {
            IEnumerable<User> users = _users.Find(predicate).ToList();
            return users;
        }

        public User Create(User user)
        {
            _users.InsertOne(user);
            return user;
        }

        public void Update(string id, User user) =>
            _users.ReplaceOneAsync(user => user.Id == id, user);

        public void Remove(User user) =>
            _users.DeleteOne(u => u.Login == user.Login);

        public void Remove(string id) =>
            _users.DeleteOne(user => user.Id == id);
    }
}
