﻿using LogBookAPI.Data.Service;
using LogBookModels.Models;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogBookStores.Service
{
    public class DocumentService: IService<Template>
    {
        private readonly IMongoCollection<Template> _document;

        public DocumentService(IDatabaseSettings settings)
        {
            var client = new MongoClient(settings.DBConnection);
            var database = client.GetDatabase(settings.DatabaseName);

            _document = database.GetCollection<Template>("Document");
        }

        public async Task<List<Template>> Get() =>
           await _document.Find(document => true).ToListAsync();


        public Template Get(string id) =>
            _document.Find<Template>(document => document.Id == id).FirstOrDefault();

        public Template Create(Template document)
        {
            document.Id = "";
            _document.InsertOne(document);
            return document;
        }

        public void Update(string id, Template document) =>
            _document.ReplaceOne(document => document.Id == id, document);

        public void Remove(Template document) =>
            _document.DeleteOne(document => document.Id == document.Id);

        public void Remove(string id) =>
            _document.DeleteOne(document => document.Id == id);
    }
}
