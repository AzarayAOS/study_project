﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;


namespace LogBookModels.Models
{
    public class Items
    {
        public List<string> Value { get; set; }
    }
}
