﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;


namespace LogBookModels.Models
{
    public enum TComponent
    {
        Edit,
        DatePicker,
        ComboBox,
        DataComboBox,
        Memo
    }

    public class Field
    {
        public string FieldLabel { get; set; }
        public byte Position { get; set; }
        public TComponent Component { get; set; }
        public string Value { get; set; }
    }
}
