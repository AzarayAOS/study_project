﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;


namespace LogBookModels.Models
{
    public class BaseDocument
    {
        public string Id { get; set; }
        public string IndexID { get; set; }
        public byte State { get; set; }


    }
}
