﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LogBookModels
{
    public interface IBaseObject
    {
        string GetId();
    }
}
