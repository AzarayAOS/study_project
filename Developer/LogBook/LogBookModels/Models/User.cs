﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Linq;

namespace LogBookModels
{
    [BsonIgnoreExtraElements]
    public class User
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement("Name")]
        public string Login { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [BsonIgnore]
        public string FullName { get { return $"{FirstName} { LastName }"; } }
        public string Phone { get; set; }
        public string CorporatePhone { get; set; }
        public bool FlagWorking { get; set; }
        public DateTime Birthday { get; set; }
        //public Company company { get; set; }
        public Role role { get; set; }
        [BsonIgnore]
        public string Password { get; set; }
        public string PasswordHash { get; set; }
        /// <summary>
        /// Токен позволяющий клиентам запрашивать новые access токены по истечеии их времени жизни. Данные токены обычно выдаются на длительный срок.
        /// </summary>
        public string RefreshToken { get; set; }
        /// <summary>
        /// Токен предоставляет доступ владельцу к защищенным ресурсам сервера, обычно имеет короткий срок жихни и может нести в себе дополнительную информацию, такую как IP адрес стороны запрашивающей данные токена
        /// </summary>
        public string AccessToken { get; set; }
    }
}
