﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace LogBookModels
{
    [BsonIgnoreExtraElements]
    public class Role : BaseObject
    {
        public string RoleName { get; set; }
        public string Description { get; set; }

        public override string ToString()
        {
            return RoleName;
        }
    }
}
