﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace LogBookModels.Models
{
    public class Document
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public byte State { get; set; }
        public string Content { get; set; }
        public byte TypeDoc { get; set; }
        public byte SignManID { get; set; }
        public DateTime DateCreate { get; set; }
        public int CreateUserID { get; set; }
        public string PrepareManPhone { get; set; }
        public int DivisionID { get; set; }
        public DateTime RegisterDate { get; set; }
    }
}
