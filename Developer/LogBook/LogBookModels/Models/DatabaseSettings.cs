﻿
namespace LogBookModels.Models
{
    public class DatabaseSettings : IDatabaseSettings
    {
        public string UsersCollectionName { get; set; }
        public string DBConnection { get; set; }
        //public string LogBookDBGlobal { get; set; }
        public string DatabaseName { get; set; }
    }

    public interface IDatabaseSettings
    {
        string UsersCollectionName { get; set; }
        string DBConnection { get; set; }
        //string LogBookDBGlobal { get; set; }
        string DatabaseName { get; set; }
    }
}
