﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogBookModels
{
    public class JWTSettings
    {
        public string SecretKey { get; set; }
    }
}
