﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogBookModels
{
    public class PatternJson
    {
        public BsonDocument Metadata { get; set; }
    }
}
